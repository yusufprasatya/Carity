//
//  NoteListViewModel.swift
//  CloudKitDemo
//
//  Created by Muhammad Yusuf on 25/05/23.
//

import Foundation
import CloudKit

class NoteListViewModel: ObservableObject{
    
    private var db: CKDatabase
    private var container: CKContainer
    
    init(container: CKContainer) {
        self.container = container
        self.db = self.container.publicCloudDatabase
    }
    
    func saveNote(note: String){
        let record = CKRecord(recordType: RecordType.Notes.rawValue)
        let note = Notes(note: note)
        record.setValuesForKeys(note.toDictionary())
        
        // SImpan data ke cloudkit database
        self.db.save(record) {newRecord, error in
            if let error = error {
                print(error)
            }else{
                if let _ = newRecord{
                    print("Simpan catatan berhasil!")
                }
            }
        }
    }
}

enum RecordType: String{
    case Notes = "Notes"
}
