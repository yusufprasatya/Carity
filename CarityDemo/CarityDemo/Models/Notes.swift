//
//  Notes.swift
//  CloudKitDemo
//
//  Created by Muhammad Yusuf on 25/05/23.
//

import Foundation
import CloudKit

struct Notes{
    var recordId: CKRecord.ID?
    let note: String
    let createdDate: Date = Date()
    
    init(recordId: CKRecord.ID? = nil, note: String) {
        self.recordId = recordId
        self.note = note
    }
    
    func toDictionary() -> [String: Any]{
        return ["note": note]
    }
}
