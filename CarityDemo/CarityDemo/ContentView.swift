//
//  ContentView.swift
//  CloudKitDemo
//
//  Created by Muhammad Yusuf on 24/05/23.
//

import SwiftUI
import CloudKit

class CloudKitUserViewModel: ObservableObject{
    
}

struct ContentView: View {
    @StateObject private var vm: NoteListViewModel
    
    @State private var notes: [CKRecord] = []
    @State private var note: String = ""
    
    init(vm: NoteListViewModel) {
        _vm = StateObject(wrappedValue: vm)
    }
    
    var body: some View {
        NavigationView{
            VStack{
                TextField("Enter note", text: $note)
                    .textFieldStyle(.roundedBorder)
                
                Button("save"){
                    vm.saveNote(note: note)
                    self.note = ""
                    
                }
                Spacer()
                    .navigationTitle("Caregiver Notes")
            }
            .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(vm: NoteListViewModel(container: CKContainer.default()))
    }
}
