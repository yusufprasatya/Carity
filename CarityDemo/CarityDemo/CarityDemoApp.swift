//
//  CarityDemoApp.swift
//  CarityDemo
//
//  Created by Muhammad Yusuf on 25/05/23.
//

import SwiftUI
import CloudKit

@main
struct CarityDemoApp: App {
    
    let container = CKContainer(identifier: "container_name")
    
    var body: some Scene {
        WindowGroup {
            ContentView(vm: NoteListViewModel(container: container))
        }
    }
}
